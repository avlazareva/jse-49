package ru.t1.lazareva.tm.dto.response;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.t1.lazareva.tm.dto.model.ProjectDto;

@Getter
@Setter
@NoArgsConstructor
public final class ProjectShowByIndexResponse extends AbstractProjectResponse {

    @Nullable
    private Integer index;

    public ProjectShowByIndexResponse(@Nullable final ProjectDto project) {
        super(project);
    }

}
