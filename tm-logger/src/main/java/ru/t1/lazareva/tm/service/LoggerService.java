package ru.t1.lazareva.tm.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.yaml.YAMLMapper;
import lombok.NoArgsConstructor;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.t1.lazareva.tm.api.ILoggerService;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.LinkedHashMap;
import java.util.Map;

@NoArgsConstructor
public final class LoggerService implements ILoggerService {

    @NotNull
    private final static String PATH = "./log/";

    @NotNull
    private final ObjectMapper objectMapper = new YAMLMapper();

    @Override
    @SneakyThrows
    public void writeLog(@NotNull final String message) {
        @NotNull final Map<String, Object> event = objectMapper.readValue(message, LinkedHashMap.class);
        @NotNull final String table = event.get("table").toString();
        @NotNull final String fileName = PATH + table + ".log";
        @NotNull final byte[] bytes = message.getBytes();
        @NotNull final File file = new File(fileName);
        if (!Files.exists(Paths.get(PATH)))
            Files.createDirectory(Paths.get(PATH));
        if (!file.exists()) file.createNewFile();
        Files.write(Paths.get(fileName), bytes, StandardOpenOption.APPEND);
    }

}
