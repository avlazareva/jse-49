package ru.t1.lazareva.tm.api.client;

import ru.t1.lazareva.tm.dto.request.AbstractRequest;
import ru.t1.lazareva.tm.dto.response.AbstractResponse;

@FunctionalInterface
public interface Operation<RQ extends AbstractRequest, RS extends AbstractResponse> {

    RS execute(RQ request);

}