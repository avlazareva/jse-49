package ru.t1.lazareva.tm.repository.dto;

import org.jetbrains.annotations.NotNull;
import ru.t1.lazareva.tm.api.repository.dto.ISessionDtoRepository;
import ru.t1.lazareva.tm.dto.model.SessionDto;

import javax.persistence.EntityManager;

public class SessionDtoRepository extends AbstractUserOwnedDtoRepository<SessionDto> implements ISessionDtoRepository {

    public SessionDtoRepository(@NotNull final EntityManager entityManager) {
        super(entityManager);
    }

    @Override
    protected Class<SessionDto> getEntityClass() {
        return SessionDto.class;
    }

}
