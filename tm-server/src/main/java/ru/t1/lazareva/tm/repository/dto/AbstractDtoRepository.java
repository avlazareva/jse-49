package ru.t1.lazareva.tm.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.lazareva.tm.api.repository.dto.IDtoRepository;
import ru.t1.lazareva.tm.comparator.CreatedComparator;
import ru.t1.lazareva.tm.comparator.NameComparator;
import ru.t1.lazareva.tm.comparator.StatusComparator;
import ru.t1.lazareva.tm.dto.model.AbstractModelDto;

import javax.persistence.EntityManager;
import java.util.Collection;
import java.util.Comparator;
import java.util.List;

public abstract class AbstractDtoRepository<M extends AbstractModelDto> implements IDtoRepository<M> {

    @NotNull
    final static String HINT = "org.hibernate.cacheable";

    @NotNull
    protected final EntityManager entityManager;

    public AbstractDtoRepository(@NotNull final EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    protected abstract Class<M> getEntityClass();

    @NotNull
    protected String getSortType(@NotNull Comparator comparator) {
        if (comparator == CreatedComparator.INSTANCE) return "created";
        if (comparator == NameComparator.INSTANCE) return "name";
        if (comparator == StatusComparator.INSTANCE) return "status";
        return "created";
    }

    @NotNull
    @Override
    public M add(@NotNull final M model) {
        entityManager.persist(model);
        return model;
    }

    @NotNull
    @Override
    public Collection<M> add(@NotNull final Collection<M> models) {
        entityManager.persist(models);
        return models;
    }

    @Override
    public void clear() throws Exception {
        for (final M model : findAll())
            entityManager.remove(model);
    }

    @Override
    public boolean existsById(@NotNull final String id) throws Exception {
        return findOneById(id) != null;
    }

    @Override
    public boolean existsByIndex(@NotNull final Integer index) throws Exception {
        return findOneByIndex(index) != null;
    }

    @Nullable
    @Override
    public List<M> findAll() {
        @NotNull final String jpql = "SELECT m FROM " + getEntityClass().getSimpleName() + " m";
        return entityManager.createQuery(jpql, getEntityClass())
                .setHint(HINT, true)
                .getResultList();
    }

    @Override
    @NotNull
    public List<M> findAll(@NotNull final Comparator<M> comparator) {
        @NotNull String query = "SELECT m FROM " + getEntityClass().getSimpleName() + " m";
        @NotNull String orderStatement = " ORDER BY m." + getSortType(comparator);
        query += orderStatement;
        return entityManager.createQuery(query, getEntityClass())
                .setHint(HINT, true)
                .getResultList();
    }

    @Nullable
    @Override
    public M findOneById(@NotNull final String id) {
        return entityManager.find(getEntityClass(), id);
    }

    @Nullable
    @Override
    public M findOneByIndex(@NotNull final Integer index) {
        return entityManager.find(getEntityClass(), index);
    }

    @Override
    public int getSize() throws Exception {
        @NotNull final String jpql = "SELECT COUNT(m) FROM " + getEntityClass().getSimpleName() + " m";
        return entityManager.createQuery(jpql, Long.class).getSingleResult().intValue();
    }

    @Override
    public void remove(@NotNull final M model) {
        entityManager.remove(model);
    }

    @Override
    public M update(@NotNull final M model) throws Exception {
        return entityManager.merge(model);
    }

}
