package ru.t1.lazareva.tm.repository.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.lazareva.tm.api.repository.model.IUserOwnedRepository;
import ru.t1.lazareva.tm.model.AbstractUserOwnedModel;
import ru.t1.lazareva.tm.model.User;

import javax.persistence.EntityManager;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;

public abstract class AbstractUserOwnedRepository<M extends AbstractUserOwnedModel> extends AbstractRepository<M> implements IUserOwnedRepository<M> {

    public AbstractUserOwnedRepository(@NotNull final EntityManager entityManager) {
        super(entityManager);
    }

    @NotNull
    @Override
    public M add(
            @NotNull final String userId,
            @NotNull final M model
    ) {
        model.setUser(entityManager.find(User.class, userId));
        return add(model);
    }

    @Override
    public void clear(@NotNull final String userId) throws Exception {
        @Nullable final List<M> models = findAll(userId);
        for (@NotNull final M model : models) {
            remove(model);
        }
    }

    @Override
    public boolean existsById(@NotNull final String userId, @NotNull final String id) throws Exception {
        return findOneById(userId, id) != null;
    }

    @Nullable
    @Override
    public List<M> findAll(@NotNull final String userId) throws Exception {
        @NotNull final String jpql = "FROM " + getClazz().getSimpleName() + " m WHERE m.user.id = :userId";
        return entityManager.createQuery(jpql, getClazz())
                .setParameter("userId", userId)
                .getResultList();
    }

    @Nullable
    @Override
    public List<M> findAll(@NotNull final String userId, @Nullable final Comparator comparator) throws Exception {
        @NotNull final String jpql = "FROM " + getClazz().getSimpleName() + " m WHERE m.user.id = :userId" + " ORDER BY " + getSortColumnName(comparator);
        return entityManager.createQuery(jpql, getClazz())
                .setParameter("userId", userId)
                .getResultList();
    }

    @Nullable
    @Override
    public M findOneById(@NotNull final String userId, @NotNull final String id) throws Exception {
        @NotNull final String jpql = "FROM " + getClazz().getSimpleName() + " m WHERE m.user.id = :userId AND m.id = :id";
        return entityManager.createQuery(jpql, getClazz())
                .setParameter("userId", userId)
                .setParameter("id", id)
                .setMaxResults(1)
                .getResultList().stream().findFirst().orElse(null);
    }

    @Nullable
    @Override
    public M findOneByIndex(@NotNull final String userId, @NotNull final Integer index) {
        @NotNull final String jpql = "FROM " + getClazz().getSimpleName() + " m WHERE m.index = :index and m.user.id = :userId";
        return entityManager.createQuery(jpql, getClazz())
                .setParameter("id", index)
                .setParameter("userId", userId)
                .setMaxResults(1)
                .getResultList()
                .stream()
                .findFirst()
                .orElse(null);
    }

    @Override
    public int getSize(@NotNull final String userId) {
        @NotNull final String jpql = "SELECT COUNT(1) FROM " + getClazz().getSimpleName() + " m " +
                "WHERE m.user.id = :userId";
        return entityManager.createQuery(jpql, Long.class)
                .setParameter("userId", userId)
                .getSingleResult()
                .intValue();
    }

    @Override
    public void remove(@NotNull final String userId, @NotNull final M model) throws Exception {
        Optional<M> modelResult = Optional.ofNullable(findOneById(userId, model.getId()));
        modelResult.ifPresent(this::remove);
    }

    @Override
    public void removeById(@NotNull final String userId, @NotNull final String id) throws Exception {
        Optional<M> model = Optional.ofNullable(findOneById(userId, id));
        model.ifPresent(this::remove);
    }

    @Nullable
    @Override
    public M update(
            @NotNull String userId,
            @NotNull M model
    ) {
        model.setUser(entityManager.find(User.class, userId));
        return entityManager.merge(model);
    }
}
