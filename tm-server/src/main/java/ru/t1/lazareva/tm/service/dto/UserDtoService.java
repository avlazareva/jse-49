package ru.t1.lazareva.tm.service.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.lazareva.tm.api.repository.dto.IUserDtoRepository;
import ru.t1.lazareva.tm.api.service.IConnectionService;
import ru.t1.lazareva.tm.api.service.IPropertyService;
import ru.t1.lazareva.tm.api.service.dto.IProjectDtoService;
import ru.t1.lazareva.tm.api.service.dto.ITaskDtoService;
import ru.t1.lazareva.tm.api.service.dto.IUserDtoService;
import ru.t1.lazareva.tm.dto.model.UserDto;
import ru.t1.lazareva.tm.enumerated.Role;
import ru.t1.lazareva.tm.exception.entity.EntityNotFoundException;
import ru.t1.lazareva.tm.exception.entity.UserNotFoundException;
import ru.t1.lazareva.tm.exception.field.EmailEmptyException;
import ru.t1.lazareva.tm.exception.field.IdEmptyException;
import ru.t1.lazareva.tm.exception.field.LoginEmptyException;
import ru.t1.lazareva.tm.exception.field.PasswordEmptyException;
import ru.t1.lazareva.tm.exception.user.ExistsEmailException;
import ru.t1.lazareva.tm.exception.user.ExistsLoginException;
import ru.t1.lazareva.tm.exception.user.RoleEmptyException;
import ru.t1.lazareva.tm.repository.dto.UserDtoRepository;
import ru.t1.lazareva.tm.util.HashUtil;

import javax.persistence.EntityManager;

public final class UserDtoService extends AbstractDtoService<UserDto, IUserDtoRepository> implements IUserDtoService {

    @NotNull
    private final IProjectDtoService projectService;

    @NotNull
    private final ITaskDtoService taskService;

    @NotNull
    private final IPropertyService propertyService;

    public UserDtoService(@NotNull final IPropertyService propertyService, @NotNull final IConnectionService connectionService, @NotNull final IProjectDtoService projectService, @NotNull final ITaskDtoService taskService) {
        super(connectionService);
        this.propertyService = propertyService;
        this.projectService = projectService;
        this.taskService = taskService;
    }

    @NotNull
    protected IUserDtoRepository getRepository(@NotNull final EntityManager entityManager) {
        return new UserDtoRepository(entityManager);
    }

    @NotNull
    @Override
    public UserDto create(@Nullable final String login, @Nullable final String password) throws Exception {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        if (isLoginExists(login)) throw new ExistsLoginException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        @NotNull UserDto user = new UserDto();
        user.setLogin(login);
        user.setPasswordHash(HashUtil.salt(propertyService, password));
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final IUserDtoRepository repository = getRepository(entityManager);
            entityManager.getTransaction().begin();
            repository.add(user);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
        return user;
    }

    @NotNull
    @Override
    public UserDto create(@Nullable final String login, @Nullable final String password, @Nullable final String email) throws Exception {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        if (isLoginExists(login)) throw new ExistsLoginException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        if (isEmailExists(email)) throw new ExistsEmailException();
        @NotNull UserDto user = new UserDto();
        user.setLogin(login);
        user.setPasswordHash(HashUtil.salt(propertyService, password));
        user.setEmail(email);
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final IUserDtoRepository repository = getRepository(entityManager);
            entityManager.getTransaction().begin();
            repository.add(user);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
        return user;
    }

    @NotNull
    @Override
    public UserDto create(@Nullable final String login, @Nullable final String password, @Nullable final Role role) throws Exception {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        if (isLoginExists(login)) throw new ExistsLoginException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        if (role == null) throw new RoleEmptyException();
        @NotNull UserDto user = new UserDto();
        user.setLogin(login);
        user.setPasswordHash(HashUtil.salt(propertyService, password));
        user.setRole(role);
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final IUserDtoRepository repository = getRepository(entityManager);
            entityManager.getTransaction().begin();
            repository.add(user);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
        return user;
    }

    @Nullable
    @Override
    public UserDto findByLogin(@Nullable final String login) throws Exception {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        @Nullable UserDto resultModel;
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final IUserDtoRepository repository = getRepository(entityManager);
            resultModel = repository.findByLogin(login);
        } finally {
            entityManager.close();
        }
        if (resultModel == null) throw new EntityNotFoundException();
        return resultModel;
    }

    @Nullable
    @Override
    public UserDto findByEmail(@Nullable final String email) throws Exception {
        if (email == null || email.isEmpty()) throw new EmailEmptyException();
        @Nullable UserDto resultModel;
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final IUserDtoRepository repository = getRepository(entityManager);
            resultModel = repository.findByEmail(email);
        } finally {
            entityManager.close();
        }
        if (resultModel == null) throw new EntityNotFoundException();
        return resultModel;
    }

    @Override
    public Boolean isLoginExists(@Nullable final String login) throws Exception {
        if (login == null || login.isEmpty()) return false;
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final IUserDtoRepository repository = getRepository(entityManager);
            return (repository.findByLogin(login) != null);
        } finally {
            entityManager.close();
        }
    }

    @Override
    public Boolean isEmailExists(@Nullable final String email) throws Exception {
        if (email == null || email.isEmpty()) return false;
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final IUserDtoRepository repository = getRepository(entityManager);
            return (repository.findByEmail(email) != null);
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void lockUserByLogin(@Nullable final String login) throws Exception {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        @Nullable final UserDto user = findByLogin(login);
        if (user == null) throw new UserNotFoundException();
        user.setLocked(true);
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final IUserDtoRepository repository = getRepository(entityManager);
            entityManager.getTransaction().begin();
            repository.update(user);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void removeByLogin(@Nullable final String login) throws Exception {
        @Nullable final UserDto user = findByLogin(login);
        if (user == null) throw new UserNotFoundException();
        @NotNull final String userId = user.getId();
        taskService.clear(userId);
        projectService.clear(userId);
        remove(user);
    }

    @Override
    public UserDto setPassword(@Nullable final String id, @Nullable final String password) throws Exception {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        @Nullable final UserDto user = findOneById(id);
        if (user == null) throw new UserNotFoundException();
        user.setPasswordHash(HashUtil.salt(propertyService, password));
        @Nullable UserDto resultModel;
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final IUserDtoRepository repository = getRepository(entityManager);
            entityManager.getTransaction().begin();
            resultModel = repository.update(user);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
        if (resultModel == null) throw new EntityNotFoundException();
        return resultModel;
    }

    @Override
    public void unlockUserByLogin(@Nullable final String login) throws Exception {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        @Nullable final UserDto user = findByLogin(login);
        if (user == null) throw new UserNotFoundException();
        user.setLocked(false);
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final IUserDtoRepository repository = getRepository(entityManager);
            entityManager.getTransaction().begin();
            repository.update(user);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    public UserDto update(@Nullable final String id, @Nullable final String firstName, @Nullable final String lastName, @Nullable final String middleName) throws Exception {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @Nullable final UserDto user = findOneById(id);
        if (user == null) throw new UserNotFoundException();
        user.setFirstName((firstName == null) ? "" : firstName);
        user.setLastName((lastName == null) ? "" : lastName);
        user.setMiddleName((middleName == null) ? "" : middleName);
        @Nullable UserDto resultModel;
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final IUserDtoRepository repository = getRepository(entityManager);
            entityManager.getTransaction().begin();
            resultModel = repository.update(user);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
        if (resultModel == null) throw new EntityNotFoundException();
        return resultModel;
    }

}
