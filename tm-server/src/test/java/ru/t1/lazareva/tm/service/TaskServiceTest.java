package ru.t1.lazareva.tm.service;

import liquibase.Liquibase;
import liquibase.exception.LiquibaseException;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.*;
import org.junit.experimental.categories.Category;
import ru.t1.lazareva.tm.api.service.IConnectionService;
import ru.t1.lazareva.tm.api.service.IPropertyService;
import ru.t1.lazareva.tm.api.service.dto.IProjectDtoService;
import ru.t1.lazareva.tm.api.service.dto.ITaskDtoService;
import ru.t1.lazareva.tm.api.service.dto.IUserDtoService;
import ru.t1.lazareva.tm.constant.ProjectTestData;
import ru.t1.lazareva.tm.dto.model.TaskDto;
import ru.t1.lazareva.tm.enumerated.Status;
import ru.t1.lazareva.tm.exception.entity.EntityNotFoundException;
import ru.t1.lazareva.tm.exception.entity.TaskNotFoundException;
import ru.t1.lazareva.tm.exception.field.DescriptionEmptyException;
import ru.t1.lazareva.tm.exception.field.IdEmptyException;
import ru.t1.lazareva.tm.exception.field.NameEmptyException;
import ru.t1.lazareva.tm.exception.field.UserIdEmptyException;
import ru.t1.lazareva.tm.marker.UnitCategory;
import ru.t1.lazareva.tm.migration.AbstractSchemeTest;
import ru.t1.lazareva.tm.service.dto.ProjectDtoService;
import ru.t1.lazareva.tm.service.dto.TaskDtoService;
import ru.t1.lazareva.tm.service.dto.UserDtoService;

import java.util.Collection;

import static ru.t1.lazareva.tm.constant.TaskTestData.*;

@Category(UnitCategory.class)
public final class TaskServiceTest extends AbstractSchemeTest {

    @NotNull
    private static final IPropertyService PROPERTY_SERVICE = new PropertyService();

    @NotNull
    private static final IConnectionService CONNECTION_SERVICE = new ConnectionService(PROPERTY_SERVICE);

    @NotNull
    private static final ITaskDtoService SERVICE = new TaskDtoService(CONNECTION_SERVICE);

    @NotNull
    private static final IProjectDtoService PROJECT_SERVICE = new ProjectDtoService(CONNECTION_SERVICE);

    @NotNull
    private static final IUserDtoService USER_SERVICE = new UserDtoService(PROPERTY_SERVICE, CONNECTION_SERVICE, PROJECT_SERVICE, SERVICE);

    @BeforeClass
    public static void changeSchema() throws LiquibaseException {
        final Liquibase liquibase = liquibase("changelog/changelog-master.xml");
        liquibase.dropAll();
        liquibase.update("scheme");
    }

    @Before
    public void before() throws Exception {
        USER_SERVICE.add(USER_1);
        USER_SERVICE.add(USER_2);
        SERVICE.add(USER_1.getId(), USER_TASK1);
        SERVICE.add(USER_2.getId(), USER_TASK2);
    }

    @After
    public void after() throws Exception {
        SERVICE.clear();
        USER_SERVICE.clear();
    }

    @Test
    public void add() throws Exception {
        SERVICE.clear();
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            SERVICE.add(null, USER_TASK1);
        });
        Assert.assertThrows(EntityNotFoundException.class, () -> {
            SERVICE.add(USER_TASK1.getUserId(), null);
        });
        SERVICE.clear();
        Assert.assertNotNull(SERVICE.add(USER_TASK1.getUserId(), USER_TASK1));
        @Nullable final TaskDto task = SERVICE.findOneById(USER_TASK1.getId());
        Assert.assertNotNull(task);
        Assert.assertEquals(USER_TASK1.getId(), task.getId());
    }

    @Test
    public void addByUserId() throws Exception {
        Assert.assertThrows(EntityNotFoundException.class, () -> {
            SERVICE.add(ProjectTestData.USER_1.getId(), NULL_TASK);
        });
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            SERVICE.add(null, USER_TASK1);
        });
        SERVICE.clear();
        Assert.assertNotNull(SERVICE.add(USER_TASK1.getUserId(), USER_TASK1));
        @Nullable final TaskDto task = SERVICE.findOneById(USER_1.getId(), USER_TASK1.getId());
        Assert.assertNotNull(task);
        Assert.assertEquals(USER_TASK1.getId(), task.getId());
    }

    @Test
    public void findAll() {
        @NotNull final Collection<TaskDto> tasksFindAllNoEmpty = SERVICE.findAll();
        Assert.assertNotNull(tasksFindAllNoEmpty);
    }

    @Test
    public void existsById() throws Exception {
        Assert.assertFalse(SERVICE.existsById(""));
        Assert.assertFalse(SERVICE.existsById(null));
        Assert.assertFalse(SERVICE.existsById(NON_EXISTING_TASK_ID));
        Assert.assertTrue(SERVICE.existsById(USER_TASK1.getId()));
    }

    @Test
    public void existsByIdByUserId() throws Exception {
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            SERVICE.existsById(null, NON_EXISTING_TASK_ID);
        });
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            SERVICE.existsById("", NON_EXISTING_TASK_ID);
        });
        Assert.assertThrows(IdEmptyException.class, () -> {
            SERVICE.existsById(USER_1.getId(), null);
        });
        Assert.assertThrows(IdEmptyException.class, () -> {
            SERVICE.existsById(USER_1.getId(), "");
        });
        Assert.assertFalse(SERVICE.existsById(USER_1.getId(), NON_EXISTING_TASK_ID));
        Assert.assertTrue(SERVICE.existsById(USER_1.getId(), USER_TASK1.getId()));
    }

    @Test
    public void findOneById() {
        Assert.assertThrows(EntityNotFoundException.class, () -> {
            SERVICE.findOneById(NON_EXISTING_TASK_ID);
        });
        @Nullable final TaskDto task = SERVICE.findOneById(USER_TASK1.getId());
        Assert.assertNotNull(task);
        Assert.assertEquals(USER_TASK1.getId(), task.getId());
    }

    @Test
    public void findOneByIdByUserId() throws Exception {
        Assert.assertThrows(IdEmptyException.class, () -> {
            SERVICE.findOneById(USER_1.getId(), null);
        });
        Assert.assertThrows(IdEmptyException.class, () -> {
            SERVICE.findOneById(USER_1.getId(), "");
        });
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            SERVICE.existsById(null, USER_TASK1.getId());
        });
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            SERVICE.existsById("", USER_TASK1.getId());
        });
        Assert.assertNull(SERVICE.findOneById(USER_1.getId(), NON_EXISTING_TASK_ID));
        @Nullable final TaskDto task = SERVICE.findOneById(USER_1.getId(), USER_TASK1.getId());
        Assert.assertNotNull(task);
        Assert.assertEquals(USER_TASK1.getId(), task.getId());
    }

    @Test
    public void clear() throws Exception {
        SERVICE.clear();
        Assert.assertEquals(0, SERVICE.getSize());
    }

    @Test
    public void clearByUserId() throws Exception {
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            SERVICE.clear(null);
        });
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            SERVICE.clear("");
        });
        SERVICE.clear();
        SERVICE.add(USER_1.getId(), USER_TASK1);
        SERVICE.clear(USER_1.getId());
        Assert.assertEquals(0, SERVICE.getSize(USER_1.getId()));
    }

    @Test
    public void remove() throws Exception {
        Assert.assertThrows(EntityNotFoundException.class, () -> {
            SERVICE.remove(null);
        });
        SERVICE.clear();
        @Nullable final TaskDto createdTask = SERVICE.create(USER_1.getId(), USER_TASK1.getName());
        SERVICE.remove(createdTask);
        Assert.assertEquals(0, SERVICE.findAll(USER_TASK1.getUserId()).size());
    }

    @Test
    public void removeByUserId() throws Exception {
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            SERVICE.remove(null, null);
        });
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            SERVICE.remove("", null);
        });
        Assert.assertThrows(EntityNotFoundException.class, () -> {
            SERVICE.remove(USER_1.getId(), null);
        });
        SERVICE.clear();
        @NotNull final TaskDto createdTask = SERVICE.create(USER_1.getId(), USER_TASK1.getName());
        SERVICE.remove(USER_1.getId(), createdTask);
        Assert.assertNull(SERVICE.findOneById(USER_1.getId(), USER_TASK1.getId()));
    }

    @Test
    public void removeByIdByUserId() throws Exception {
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            SERVICE.removeById(null, null);
        });
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            SERVICE.removeById("", null);
        });
        Assert.assertThrows(IdEmptyException.class, () -> {
            SERVICE.removeById(USER_1.getId(), null);
        });
        Assert.assertThrows(IdEmptyException.class, () -> {
            SERVICE.removeById(USER_1.getId(), "");
        });
        SERVICE.removeById(USER_1.getId(), USER_TASK1.getId());
        Assert.assertNull(SERVICE.findOneById(ProjectTestData.USER_1.getId(), USER_TASK1.getId()));
        @Nullable final TaskDto createdTask = SERVICE.add(USER_TASK1);
        SERVICE.removeById(USER_1.getId(), createdTask.getId());
        Assert.assertNull(SERVICE.findOneById(USER_1.getId(), USER_TASK1.getId()));
    }

    @Test
    public void getSize() throws Exception {
        SERVICE.clear();
        SERVICE.add(USER_TASK1);
        Assert.assertEquals(1, SERVICE.getSize());
    }

    @Test
    public void getSizeByUserId() throws Exception {
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            SERVICE.getSize(null);
        });
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            SERVICE.getSize("");
        });
        SERVICE.clear();
        Assert.assertTrue(SERVICE.findAll().isEmpty());
        Assert.assertEquals(0, SERVICE.getSize(USER_1.getId()));
        SERVICE.add(USER_TASK1);
        Assert.assertEquals(1, SERVICE.getSize(USER_1.getId()));
    }

    @Test
    public void create() throws Exception {
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            SERVICE.create(null, USER_TASK1.getName());
        });
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            SERVICE.create("", USER_TASK1.getName());
        });
        Assert.assertThrows(NameEmptyException.class, () -> {
            SERVICE.create(ProjectTestData.USER_1.getId(), null);
        });
        Assert.assertThrows(NameEmptyException.class, () -> {
            SERVICE.create(ProjectTestData.USER_1.getId(), "");
        });
        @NotNull final TaskDto task = SERVICE.create(USER_1.getId(), USER_TASK1.getName());
        Assert.assertEquals(task.getId(), SERVICE.findOneById(USER_1.getId(), task.getId()).getId());
        Assert.assertEquals(USER_TASK1.getName(), task.getName());
        Assert.assertEquals(USER_1.getId(), task.getUserId());
    }

    @Test
    public void createWithDescription() throws Exception {
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            SERVICE.create(null, USER_TASK1.getName(), USER_TASK1.getDescription());
        });
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            SERVICE.create("", USER_TASK1.getName(), USER_TASK1.getDescription());
        });
        Assert.assertThrows(NameEmptyException.class, () -> {
            SERVICE.create(USER_1.getId(), null, USER_TASK1.getDescription());
        });
        Assert.assertThrows(NameEmptyException.class, () -> {
            SERVICE.create(USER_1.getId(), "", USER_TASK1.getDescription());
        });
        Assert.assertThrows(DescriptionEmptyException.class, () -> {
            SERVICE.create(USER_1.getId(), USER_TASK1.getName(), null);
        });
        Assert.assertThrows(DescriptionEmptyException.class, () -> {
            SERVICE.create(USER_1.getId(), USER_TASK1.getName(), "");
        });
        @NotNull final TaskDto task = SERVICE.create(USER_1.getId(), USER_TASK1.getName(), USER_TASK1.getDescription());
        Assert.assertEquals(task.getId(), SERVICE.findOneById(USER_1.getId(), task.getId()).getId());
        Assert.assertEquals(USER_TASK1.getName(), task.getName());
        Assert.assertEquals(USER_TASK1.getDescription(), task.getDescription());
        Assert.assertEquals(USER_1.getId(), task.getUserId());
    }

    @Test
    public void updateById() throws Exception {
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            SERVICE.updateById(null, USER_TASK1.getId(), USER_TASK1.getName(), USER_TASK1.getDescription());
        });
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            SERVICE.updateById("", USER_TASK1.getId(), USER_TASK1.getName(), USER_TASK1.getDescription());
        });
        Assert.assertThrows(IdEmptyException.class, () -> {
            SERVICE.updateById(USER_1.getId(), null, USER_TASK1.getName(), USER_TASK1.getDescription());
        });
        Assert.assertThrows(IdEmptyException.class, () -> {
            SERVICE.updateById(USER_1.getId(), "", USER_TASK1.getName(), USER_TASK1.getDescription());
        });
        Assert.assertThrows(NameEmptyException.class, () -> {
            SERVICE.updateById(USER_1.getId(), USER_TASK1.getId(), null, USER_TASK1.getDescription());
        });
        Assert.assertThrows(NameEmptyException.class, () -> {
            SERVICE.updateById(USER_1.getId(), USER_TASK1.getId(), "", USER_TASK1.getDescription());
        });
        Assert.assertThrows(TaskNotFoundException.class, () -> {
            SERVICE.updateById(USER_1.getId(), NON_EXISTING_TASK_ID, USER_TASK1.getName(), USER_TASK1.getDescription());
        });
        @NotNull final String name = USER_TASK1.getName();
        @NotNull final String description = USER_TASK1.getDescription();
        SERVICE.updateById(USER_1.getId(), USER_TASK1.getId(), name, description);
        Assert.assertEquals(name, USER_TASK1.getName());
        Assert.assertEquals(description, USER_TASK1.getDescription());
    }

    @Test
    public void changeTaskStatusById() throws Exception {
        @NotNull final Status status = Status.NOT_STARTED;
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            SERVICE.changeTaskStatusById(null, USER_TASK1.getId(), status);
        });
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            SERVICE.changeTaskStatusById("", USER_TASK1.getId(), status);
        });
        Assert.assertThrows(IdEmptyException.class, () -> {
            SERVICE.changeTaskStatusById(USER_1.getId(), null, status);
        });
        Assert.assertThrows(IdEmptyException.class, () -> {
            SERVICE.changeTaskStatusById(USER_1.getId(), "", status);
        });
        Assert.assertThrows(TaskNotFoundException.class, () -> {
            SERVICE.changeTaskStatusById(USER_1.getId(), NON_EXISTING_TASK_ID, status);
        });
        SERVICE.changeTaskStatusById(USER_1.getId(), USER_TASK1.getId(), status);
        Assert.assertNotNull(USER_TASK1);
        Assert.assertEquals(status, USER_TASK1.getStatus());
    }

}

