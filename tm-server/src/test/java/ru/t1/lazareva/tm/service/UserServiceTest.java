package ru.t1.lazareva.tm.service;

import liquibase.Liquibase;
import liquibase.exception.LiquibaseException;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.*;
import org.junit.experimental.categories.Category;
import ru.t1.lazareva.tm.api.service.IConnectionService;
import ru.t1.lazareva.tm.api.service.IPropertyService;
import ru.t1.lazareva.tm.api.service.dto.IDtoService;
import ru.t1.lazareva.tm.api.service.dto.IProjectDtoService;
import ru.t1.lazareva.tm.api.service.dto.ITaskDtoService;
import ru.t1.lazareva.tm.api.service.dto.IUserDtoService;
import ru.t1.lazareva.tm.dto.model.UserDto;
import ru.t1.lazareva.tm.enumerated.Role;
import ru.t1.lazareva.tm.exception.entity.EntityNotFoundException;
import ru.t1.lazareva.tm.exception.field.EmailEmptyException;
import ru.t1.lazareva.tm.exception.field.IdEmptyException;
import ru.t1.lazareva.tm.exception.field.LoginEmptyException;
import ru.t1.lazareva.tm.exception.field.PasswordEmptyException;
import ru.t1.lazareva.tm.exception.user.RoleEmptyException;
import ru.t1.lazareva.tm.marker.UnitCategory;
import ru.t1.lazareva.tm.migration.AbstractSchemeTest;
import ru.t1.lazareva.tm.service.dto.ProjectDtoService;
import ru.t1.lazareva.tm.service.dto.SessionDtoService;
import ru.t1.lazareva.tm.service.dto.TaskDtoService;
import ru.t1.lazareva.tm.service.dto.UserDtoService;
import ru.t1.lazareva.tm.util.HashUtil;

import java.util.Collection;

import static ru.t1.lazareva.tm.constant.UserTestData.*;

@Category(UnitCategory.class)
public final class UserServiceTest extends AbstractSchemeTest {

    @NotNull
    private static final IPropertyService PROPERTY_SERVICE = new PropertyService();

    @NotNull
    private static final IConnectionService CONNECTION_SERVICE = new ConnectionService(PROPERTY_SERVICE);

    @NotNull
    private static final IDtoService DTO_SERVICE = new SessionDtoService(CONNECTION_SERVICE);

    @NotNull
    private static final ITaskDtoService TASK_SERVICE = new TaskDtoService(CONNECTION_SERVICE);

    @NotNull
    private static final IProjectDtoService PROJECT_SERVICE = new ProjectDtoService(CONNECTION_SERVICE);

    @NotNull
    private static final IUserDtoService SERVICE = new UserDtoService(PROPERTY_SERVICE, CONNECTION_SERVICE, PROJECT_SERVICE, TASK_SERVICE);

    @NotNull
    private static final IUserDtoService EMPTY_SERVICE = new UserDtoService(PROPERTY_SERVICE, CONNECTION_SERVICE, PROJECT_SERVICE, TASK_SERVICE);

    @BeforeClass
    public static void changeSchema() throws LiquibaseException {
        final Liquibase liquibase = liquibase("changelog/changelog-master.xml");
        liquibase.dropAll();
        liquibase.update("scheme");
    }

    @Before
    public void before() throws Exception {
        SERVICE.add(USER_1);
        SERVICE.add(USER_2);
    }

    @After
    public void after() throws Exception {
        SERVICE.clear();
    }

    @Test
    public void add() throws Exception {
        SERVICE.clear();
        @Nullable final UserDto user = SERVICE.add(USER_1);
        Assert.assertNotNull(user);
        @Nullable final String userId = SERVICE.findOneById(user.getId()).getId();
        Assert.assertNotNull(userId);
        Assert.assertEquals(userId, USER_1.getId());
    }

    @Test
    public void findAll() {
        @NotNull final Collection<UserDto> userFindAllNoEmpty = SERVICE.findAll();
        Assert.assertNotNull(userFindAllNoEmpty);
    }

    @Test
    public void existsById() throws Exception {
        Assert.assertFalse(SERVICE.existsById(""));
        Assert.assertFalse(SERVICE.existsById(null));
        Assert.assertFalse(SERVICE.existsById(NON_EXISTING_USER_ID));
        Assert.assertTrue(SERVICE.existsById(USER_1.getId()));
    }

    @Test
    public void findOneById() throws Exception {
        Assert.assertThrows(EntityNotFoundException.class, () -> {
            SERVICE.findOneById(NON_EXISTING_USER_ID);
        });
        @Nullable final UserDto user = SERVICE.findOneById(USER_1.getId());
        Assert.assertNotNull(user);
        Assert.assertEquals(USER_1.getId(), user.getId());
    }

    @Test
    public void clear() throws Exception {
        SERVICE.clear();
        Assert.assertEquals(0, SERVICE.getSize());
    }

    @Test
    public void removeById() throws Exception {
        Assert.assertThrows(IdEmptyException.class, () -> {
            SERVICE.removeById(null);
        });
        Assert.assertThrows(IdEmptyException.class, () -> {
            SERVICE.removeById("");
        });
        Assert.assertThrows(EntityNotFoundException.class, () -> {
            SERVICE.removeById(NON_EXISTING_USER_ID);
        });
        SERVICE.clear();
        @Nullable final UserDto createdUser = SERVICE.add(USER_1);
        SERVICE.remove(createdUser);
        Assert.assertThrows(EntityNotFoundException.class, () -> SERVICE.findOneById(USER_1.getId()));
    }

    @Test
    public void getSize() throws Exception {
        SERVICE.clear();
        SERVICE.add(USER_1);
        Assert.assertEquals(1, SERVICE.getSize());
    }

    @Test
    public void create() throws Exception {
        SERVICE.clear();
        Assert.assertThrows(LoginEmptyException.class, () -> {
            SERVICE.create(null, ADMIN_TEST_PASSWORD);
        });
        Assert.assertThrows(LoginEmptyException.class, () -> {
            SERVICE.create("", ADMIN_TEST_PASSWORD);
        });
        Assert.assertThrows(PasswordEmptyException.class, () -> {
            SERVICE.create(ADMIN_TEST_LOGIN, null);
        });
        Assert.assertThrows(PasswordEmptyException.class, () -> {
            SERVICE.create(ADMIN_TEST_LOGIN, "");
        });
        SERVICE.clear();
        @NotNull final UserDto user = SERVICE.create(ADMIN_TEST_LOGIN, ADMIN_TEST_PASSWORD);
        Assert.assertNotNull(user);
        Assert.assertEquals(ADMIN_TEST_LOGIN, user.getLogin());
        Assert.assertEquals(HashUtil.salt(PROPERTY_SERVICE, ADMIN_TEST_PASSWORD), user.getPasswordHash());
    }

    @Test
    public void createWithEmail() throws Exception {
        SERVICE.clear();
        Assert.assertThrows(LoginEmptyException.class, () -> {
            SERVICE.create(null, ADMIN_TEST_PASSWORD, ADMIN_TEST_EMAIL);
        });
        Assert.assertThrows(LoginEmptyException.class, () -> {
            SERVICE.create("", ADMIN_TEST_PASSWORD, ADMIN_TEST_EMAIL);
        });
        Assert.assertThrows(PasswordEmptyException.class, () -> {
            SERVICE.create(ADMIN_TEST_LOGIN, null, ADMIN_TEST_EMAIL);
        });
        Assert.assertThrows(PasswordEmptyException.class, () -> {
            SERVICE.create(ADMIN_TEST_LOGIN, "", ADMIN_TEST_EMAIL);
        });
        @NotNull final UserDto user = SERVICE.create(ADMIN_TEST_LOGIN, ADMIN_TEST_PASSWORD, ADMIN_TEST_EMAIL);
        Assert.assertNotNull(user);
        Assert.assertEquals(ADMIN_TEST_LOGIN, user.getLogin());
        Assert.assertEquals(HashUtil.salt(PROPERTY_SERVICE, ADMIN_TEST_PASSWORD), user.getPasswordHash());
        Assert.assertEquals(ADMIN_TEST_EMAIL, user.getEmail());
    }

    @Test
    public void createWithRole() throws Exception {
        SERVICE.clear();
        @NotNull final Role role = Role.ADMIN;
        Assert.assertThrows(LoginEmptyException.class, () -> {
            SERVICE.create(null, ADMIN_TEST_PASSWORD, role);
        });
        Assert.assertThrows(LoginEmptyException.class, () -> {
            SERVICE.create("", ADMIN_TEST_PASSWORD, role);
        });
        Assert.assertThrows(PasswordEmptyException.class, () -> {
            SERVICE.create(ADMIN_TEST_LOGIN, null, role);
        });
        Assert.assertThrows(PasswordEmptyException.class, () -> {
            SERVICE.create(ADMIN_TEST_LOGIN, "", role);
        });
        Assert.assertThrows(RoleEmptyException.class, () -> {
            @NotNull final Role nullRole = null;
            SERVICE.create(ADMIN_TEST_LOGIN, ADMIN_TEST_PASSWORD, nullRole);
        });
        @NotNull final UserDto user = SERVICE.create(ADMIN_TEST_LOGIN, ADMIN_TEST_PASSWORD, Role.ADMIN);
        Assert.assertNotNull(user);
        Assert.assertEquals(ADMIN_TEST_LOGIN, user.getLogin());
        Assert.assertEquals(HashUtil.salt(PROPERTY_SERVICE, ADMIN_TEST_PASSWORD), user.getPasswordHash());
        Assert.assertEquals(Role.ADMIN, user.getRole());
    }

    @Test
    public void findByLogin() throws Exception {
        Assert.assertThrows(LoginEmptyException.class, () -> {
            SERVICE.findByLogin(null);
        });
        Assert.assertThrows(LoginEmptyException.class, () -> {
            SERVICE.findByLogin("");
        });
        SERVICE.clear();
        @NotNull final UserDto userCreated = SERVICE.create(USER_TEST_LOGIN, USER_TEST_PASSWORD, USER_TEST_EMAIL);
        @Nullable final UserDto userLogin = SERVICE.findByLogin(userCreated.getLogin());
        Assert.assertNotNull(userLogin);
        Assert.assertEquals(userCreated.getEmail(), userLogin.getEmail());
    }

    @Test
    public void findByEmail() throws Exception {
        Assert.assertThrows(EmailEmptyException.class, () -> {
            SERVICE.findByEmail(null);
        });
        Assert.assertThrows(EmailEmptyException.class, () -> {
            SERVICE.findByEmail("");
        });
        SERVICE.clear();
        @NotNull final UserDto userCreated = SERVICE.create(USER_TEST_LOGIN, USER_TEST_PASSWORD, USER_TEST_EMAIL);
        @Nullable final UserDto userEmail = SERVICE.findByEmail(userCreated.getEmail());
        Assert.assertNotNull(userEmail);
        Assert.assertEquals(userCreated.getEmail(), userEmail.getEmail());
    }

    @Test
    public void remove() throws Exception {
        Assert.assertThrows(EntityNotFoundException.class, () -> {
            SERVICE.remove(null);
        });
        SERVICE.clear();
        @Nullable final UserDto createdUser = SERVICE.add(USER_1);
        SERVICE.remove(createdUser);
        Assert.assertThrows(EntityNotFoundException.class, () -> SERVICE.findOneById(USER_1.getId()));
    }

    @Test
    public void removeByLogin() throws Exception {
        Assert.assertThrows(LoginEmptyException.class, () -> {
            SERVICE.removeByLogin(null);
        });
        Assert.assertThrows(LoginEmptyException.class, () -> {
            SERVICE.removeByLogin("");
        });
        Assert.assertThrows(EntityNotFoundException.class, () -> {
            SERVICE.removeByLogin(NON_EXISTING_USER_ID);
        });
        SERVICE.clear();
        SERVICE.add(USER_1);
        SERVICE.removeByLogin(USER_TEST_LOGIN);
        Assert.assertThrows(EntityNotFoundException.class, () -> SERVICE.findOneById(USER_1.getId()));
    }

    @Test
    public void setPassword() throws Exception {
        Assert.assertThrows(IdEmptyException.class, () -> {
            SERVICE.setPassword(null, ADMIN_TEST_PASSWORD);
        });
        Assert.assertThrows(IdEmptyException.class, () -> {
            SERVICE.setPassword("", ADMIN_TEST_PASSWORD);
        });
        Assert.assertThrows(PasswordEmptyException.class, () -> {
            SERVICE.setPassword(USER_1.getId(), null);
        });
        Assert.assertThrows(PasswordEmptyException.class, () -> {
            SERVICE.setPassword(USER_1.getId(), "");
        });
        Assert.assertThrows(EntityNotFoundException.class, () -> {
            SERVICE.setPassword(NON_EXISTING_USER_ID, ADMIN_TEST_PASSWORD);
        });
        SERVICE.clear();
        @NotNull final UserDto user = SERVICE.create(ADMIN_TEST_LOGIN, ADMIN_TEST_PASSWORD);
        @Nullable final UserDto userUpdated = SERVICE.setPassword(user.getId(), USER_TEST_PASSWORD);
        Assert.assertNotNull(userUpdated);
        Assert.assertEquals(userUpdated.getId(), user.getId());
        Assert.assertEquals(HashUtil.salt(PROPERTY_SERVICE, USER_TEST_PASSWORD), userUpdated.getPasswordHash());
    }

    @Test
    public void updateUser() throws Exception {
        @NotNull final String firstName = "User_first_name";
        @NotNull final String lastName = "User_last_name";
        @NotNull final String middleName = "User_middle_name";
        Assert.assertThrows(IdEmptyException.class, () -> {
            SERVICE.update(null, firstName, lastName, middleName);
        });
        Assert.assertThrows(IdEmptyException.class, () -> {
            SERVICE.update("", firstName, lastName, middleName);
        });
        @NotNull final UserDto user = SERVICE.update(USER_1.getId(), firstName, lastName, middleName);
        Assert.assertEquals(firstName, user.getFirstName());
        Assert.assertEquals(lastName, user.getLastName());
        Assert.assertEquals(middleName, user.getMiddleName());
    }

    @Test
    public void isLoginExists() throws Exception {
        Assert.assertFalse(SERVICE.isLoginExists(null));
        Assert.assertFalse(SERVICE.isLoginExists(""));
        Assert.assertTrue(SERVICE.isLoginExists(USER_TEST_LOGIN));
    }

    @Test
    public void isEmailExists() throws Exception {
        Assert.assertFalse(SERVICE.isEmailExists(null));
        Assert.assertFalse(SERVICE.isEmailExists(""));
        Assert.assertTrue(SERVICE.isEmailExists(USER_TEST_EMAIL));
    }

    @Test
    public void lockUserByLogin() throws Exception {
        Assert.assertThrows(LoginEmptyException.class, () -> {
            SERVICE.lockUserByLogin(null);
        });
        Assert.assertThrows(LoginEmptyException.class, () -> {
            SERVICE.lockUserByLogin("");
        });
        Assert.assertThrows(EntityNotFoundException.class, () -> {
            SERVICE.lockUserByLogin(NON_EXISTING_USER_LOGIN);
        });
        SERVICE.lockUserByLogin(USER_1.getLogin());
        @Nullable final UserDto user = SERVICE.findOneById(USER_1.getId());
        Assert.assertNotNull(user);
        Assert.assertEquals(user.getId(), USER_1.getId());
        Assert.assertTrue(user.getLocked());
    }

    @Test
    public void unlockUserByLogin() throws Exception {
        Assert.assertThrows(LoginEmptyException.class, () -> {
            SERVICE.unlockUserByLogin(null);
        });
        Assert.assertThrows(LoginEmptyException.class, () -> {
            SERVICE.unlockUserByLogin("");
        });
        Assert.assertThrows(EntityNotFoundException.class, () -> {
            SERVICE.unlockUserByLogin(NON_EXISTING_USER_ID);
        });
        SERVICE.clear();
        SERVICE.add(USER_1);
        SERVICE.lockUserByLogin(USER_TEST_LOGIN);
        SERVICE.unlockUserByLogin(USER_TEST_LOGIN);
        Assert.assertFalse(USER_1.getLocked());
    }

}
