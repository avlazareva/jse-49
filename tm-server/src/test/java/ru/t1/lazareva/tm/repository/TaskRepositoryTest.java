package ru.t1.lazareva.tm.repository;

import liquibase.Liquibase;
import liquibase.exception.LiquibaseException;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.*;
import org.junit.experimental.categories.Category;
import ru.t1.lazareva.tm.api.repository.dto.IDtoRepository;
import ru.t1.lazareva.tm.api.repository.dto.ITaskDtoRepository;
import ru.t1.lazareva.tm.api.service.IConnectionService;
import ru.t1.lazareva.tm.api.service.IPropertyService;
import ru.t1.lazareva.tm.api.service.dto.IProjectDtoService;
import ru.t1.lazareva.tm.api.service.dto.ITaskDtoService;
import ru.t1.lazareva.tm.api.service.dto.IUserDtoService;
import ru.t1.lazareva.tm.dto.model.TaskDto;
import ru.t1.lazareva.tm.marker.UnitCategory;
import ru.t1.lazareva.tm.migration.AbstractSchemeTest;
import ru.t1.lazareva.tm.repository.dto.ProjectDtoRepository;
import ru.t1.lazareva.tm.repository.dto.TaskDtoRepository;
import ru.t1.lazareva.tm.service.ConnectionService;
import ru.t1.lazareva.tm.service.PropertyService;
import ru.t1.lazareva.tm.service.dto.ProjectDtoService;
import ru.t1.lazareva.tm.service.dto.TaskDtoService;
import ru.t1.lazareva.tm.service.dto.UserDtoService;

import javax.persistence.EntityManager;

import static ru.t1.lazareva.tm.constant.ProjectTestData.NON_EXISTING_PROJECT_ID;
import static ru.t1.lazareva.tm.constant.TaskTestData.*;

@Category(UnitCategory.class)
public final class TaskRepositoryTest extends AbstractSchemeTest {

    @NotNull
    private static final IPropertyService PROPERTY_SERVICE = new PropertyService();

    @NotNull
    private static final IConnectionService CONNECTION_SERVICE = new ConnectionService(PROPERTY_SERVICE);

    @NotNull
    private static final EntityManager ENTITY_MANAGER = CONNECTION_SERVICE.getEntityManager();

    @NotNull
    private static final ITaskDtoRepository REPOSITORY = new TaskDtoRepository(ENTITY_MANAGER);

    @NotNull
    private static final ITaskDtoRepository EMPTY_REPOSITORY = new TaskDtoRepository(ENTITY_MANAGER);

    @NotNull
    private static final IDtoRepository DTO_REPOSITORY = new ProjectDtoRepository(ENTITY_MANAGER);

    @NotNull
    private static final IProjectDtoService SERVICE = new ProjectDtoService(CONNECTION_SERVICE);

    @NotNull
    private static final ITaskDtoService TASK_SERVICE = new TaskDtoService(CONNECTION_SERVICE);

    @NotNull
    private static final IUserDtoService USER_SERVICE = new UserDtoService(PROPERTY_SERVICE, CONNECTION_SERVICE, SERVICE, TASK_SERVICE);

    @BeforeClass
    public static void changeSchema() throws LiquibaseException {
        final Liquibase liquibase = liquibase("changelog/changelog-master.xml");
        liquibase.dropAll();
        liquibase.update("scheme");
    }

    @BeforeClass
    @SneakyThrows
    public static void beforeClazz() {
        USER_SERVICE.add(USER_1);
        USER_SERVICE.add(USER_2);
    }

    @AfterClass
    @SneakyThrows
    public static void afterClazz() {
        USER_SERVICE.remove(USER_1);
        USER_SERVICE.remove(USER_2);
        ENTITY_MANAGER.close();
    }

    @Before
    @SneakyThrows
    public void before() {
        if (ENTITY_MANAGER.getTransaction().isActive())
            ENTITY_MANAGER.getTransaction().rollback();
    }

    @After
    @SneakyThrows
    public void after() {
        for (@NotNull final TaskDto task : TASK_LIST) {
            try {
                ENTITY_MANAGER.getTransaction().begin();
                REPOSITORY.remove(task);
                ENTITY_MANAGER.getTransaction().commit();
            } catch (@NotNull final Exception e) {
                ENTITY_MANAGER.getTransaction().rollback();
            }
        }

        try {
            ENTITY_MANAGER.getTransaction().begin();
            REPOSITORY.clear(USER_1.getId());
            ENTITY_MANAGER.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            ENTITY_MANAGER.getTransaction().rollback();
        }

        try {
            ENTITY_MANAGER.getTransaction().begin();
            REPOSITORY.clear(USER_2.getId());
            ENTITY_MANAGER.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            ENTITY_MANAGER.getTransaction().rollback();
        }
    }

    @Test
    public void add() {
        ENTITY_MANAGER.getTransaction().begin();
        REPOSITORY.add(USER_TASK1);
        ENTITY_MANAGER.getTransaction().commit();
        @Nullable final TaskDto task = REPOSITORY.findOneById(USER_TASK1.getId());
        Assert.assertNotNull(task);
        Assert.assertEquals(USER_TASK1.getId(), task.getId());
    }

    @Test
    public void findAll() {
        ENTITY_MANAGER.getTransaction().begin();
        REPOSITORY.add(USER_TASK1);
        ENTITY_MANAGER.getTransaction().commit();
        Assert.assertNotNull(REPOSITORY.findAll());
    }

    @Test
    public void existsById() throws Exception {
        @NotNull final TaskDto createdTask = USER_TASK1;
        ENTITY_MANAGER.getTransaction().begin();
        EMPTY_REPOSITORY.add(createdTask);
        ENTITY_MANAGER.getTransaction().commit();
        Assert.assertFalse(REPOSITORY.existsById(NON_EXISTING_PROJECT_ID));
        Assert.assertTrue(REPOSITORY.existsById(USER_TASK1.getId()));
    }

    @Test
    public void existsByIdByUserId() throws Exception {
        @NotNull final TaskDto createdTask = USER_TASK1;
        ENTITY_MANAGER.getTransaction().begin();
        EMPTY_REPOSITORY.add(createdTask);
        ENTITY_MANAGER.getTransaction().commit();
        Assert.assertFalse(REPOSITORY.existsById(USER_1.getId(), NON_EXISTING_TASK_ID));
        Assert.assertTrue(REPOSITORY.existsById(USER_1.getId(), USER_TASK1.getId()));
    }

    @Test
    public void findOneById() throws Exception {
        ENTITY_MANAGER.getTransaction().begin();
        REPOSITORY.add(USER_TASK1);
        ENTITY_MANAGER.getTransaction().commit();
        @Nullable final TaskDto task1 = REPOSITORY.findOneById(USER_TASK1.getId());
        Assert.assertNotNull(task1);
        Assert.assertEquals(USER_TASK1.getId(), task1.getId());
        @Nullable final TaskDto task2 = REPOSITORY.findOneById("");
        Assert.assertNull(task2);
    }

    @Test
    public void findOneByIdByUserId() throws Exception {
        @NotNull final TaskDto createdTask = USER_TASK1;
        ENTITY_MANAGER.getTransaction().begin();
        REPOSITORY.add(USER_1.getId(), createdTask);
        ENTITY_MANAGER.getTransaction().commit();
        Assert.assertNull(REPOSITORY.findOneById(USER_1.getId(), NON_EXISTING_TASK_ID));
        @Nullable final TaskDto task = REPOSITORY.findOneById(USER_1.getId(), createdTask.getId());
        Assert.assertNotNull(task);
        Assert.assertEquals(createdTask, task);
        ENTITY_MANAGER.getTransaction().begin();
        REPOSITORY.remove(createdTask);
        ENTITY_MANAGER.getTransaction().commit();
    }

    @Test
    public void clear() throws Exception {
        @NotNull final TaskDto createdTask = USER_TASK1;
        ENTITY_MANAGER.getTransaction().begin();
        REPOSITORY.add(createdTask);
        ENTITY_MANAGER.getTransaction().commit();
        ENTITY_MANAGER.getTransaction().begin();
        REPOSITORY.clear(USER_TASK1.getUserId());
        ENTITY_MANAGER.getTransaction().commit();
        Assert.assertEquals(0, REPOSITORY.getSize());
    }

    @Test
    public void clearByUserId() throws Exception {
        ENTITY_MANAGER.getTransaction().begin();
        REPOSITORY.add(USER_TASK1);
        REPOSITORY.add(USER_TASK2);
        ENTITY_MANAGER.getTransaction().commit();
        ENTITY_MANAGER.getTransaction().begin();
        REPOSITORY.clear(USER_1.getId());
        ENTITY_MANAGER.getTransaction().commit();
        Assert.assertEquals(0, REPOSITORY.getSize(USER_1.getId()));
    }

    public void remove() {
        ENTITY_MANAGER.getTransaction().begin();
        @Nullable final TaskDto createdTask = REPOSITORY.add(USER_TASK1);
        ENTITY_MANAGER.getTransaction().commit();
        Assert.assertNotNull(createdTask);
        ENTITY_MANAGER.getTransaction().begin();
        REPOSITORY.remove(createdTask);
        ENTITY_MANAGER.getTransaction().commit();
        @Nullable final TaskDto task = REPOSITORY.findOneById(USER_TASK1.getId());
        Assert.assertNull(task);
    }

    @Test
    public void removeByUserId() throws Exception {
        ENTITY_MANAGER.getTransaction().begin();
        @Nullable final TaskDto createdTask = REPOSITORY.add(USER_TASK1);
        ENTITY_MANAGER.getTransaction().commit();
        Assert.assertNotNull(createdTask);
        ENTITY_MANAGER.getTransaction().begin();
        REPOSITORY.remove(USER_1.getId(), createdTask);
        ENTITY_MANAGER.getTransaction().commit();
        @Nullable final TaskDto task = REPOSITORY.findOneById(USER_1.getId(), USER_TASK1.getId());
        Assert.assertNull(task);
    }

    @Test
    public void removeByIdByUserId() throws Exception {
        ENTITY_MANAGER.getTransaction().begin();
        @Nullable final TaskDto createdTask = REPOSITORY.add(USER_TASK1);
        ENTITY_MANAGER.getTransaction().commit();
        Assert.assertNotNull(createdTask);
        ENTITY_MANAGER.getTransaction().begin();
        REPOSITORY.removeById(USER_1.getId(), USER_TASK1.getId());
        ENTITY_MANAGER.getTransaction().commit();
        @Nullable final TaskDto task = REPOSITORY.findOneById(USER_1.getId(), USER_TASK1.getId());
        Assert.assertNull(task);
    }

    @Test
    public void getSize() throws Exception {
        Assert.assertTrue(REPOSITORY.findAll().isEmpty());
        Assert.assertEquals(0, REPOSITORY.getSize());
        ENTITY_MANAGER.getTransaction().begin();
        REPOSITORY.add(USER_TASK1);
        ENTITY_MANAGER.getTransaction().commit();
        Assert.assertEquals(1, REPOSITORY.getSize());
    }

    @Test
    public void getSizeByUserId() throws Exception {
        Assert.assertTrue(REPOSITORY.findAll().isEmpty());
        Assert.assertEquals(0, REPOSITORY.getSize(USER_TASK1.getUserId()));
        ENTITY_MANAGER.getTransaction().begin();
        REPOSITORY.add(USER_TASK1);
        ENTITY_MANAGER.getTransaction().commit();
        Assert.assertEquals(1, REPOSITORY.getSize(USER_TASK1.getUserId()));
    }

}
